--Clanovi: Haris Osmanbegovic, Irhad Halilovic, Mirza Muharemovic i Rasim Sabanovic

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity CPU is
	port (
		clk : in std_logic;  
		reset : in std_logic;
		adresa : out std_logic_vector (15 downto 0); --izlaz iz MAR
		podaci : inout std_logic_vector (15 downto 0); --port na MBR
-- preko signal podaci procesor �alje ili dobavlja podatke od U/I ure�aja
		rd : out std_logic;
		wr : out std_logic
	);
end CPU;

architecture Behavioral of CPU is
	component distributer is
		port (
		clk, reset : in std_logic;
		t1, t2, t3, t4 : out std_logic
	);
	end component;
	
	component amux is
	port(
		kontrola : in std_logic; --amux iz instrukcije
		alatch, mbr : in std_logic_vector(15 downto 0); 
		izlaz : out std_logic_vector(15 downto 0) --ide u alu
		);
	end component;
	
	component alu is
	port(
		ulaz : in std_logic_vector (1 downto 0); --odabir operacije
		negative_flag, zero_flag : out std_logic; --n i z flegovi
		alatch, blatch : in std_logic_vector (15 downto 0); --ulazi u alu
		izlaz : out std_logic_vector (15 downto 0) --ulaz u shifter
	);
	end component;
	
	component shifter is
	port(
			ulaz : in std_logic_vector (15 downto 0); --izlaz iz alu
			izlaz : out std_logic_vector (15 downto 0); --ide na c sabirnicu
			s : in std_logic_vector(1 downto 0) --kontrola shiftanja (desno/lijevo)
		);
	end component;
	
	component registri is
	port (
		a_adr : in std_logic_vector (3 downto 0);
		b_adr : in std_logic_vector (3 downto 0);
		c_adr : in std_logic_vector (3 downto 0);
		
		a_bus : out std_logic_vector (15 downto 0);
		b_bus : out std_logic_vector (15 downto 0);
		c_bus : inout std_logic_vector (15 downto 0);
		enc : in std_logic;
		clock : in std_logic --cetvrti ciklus s_t4
	);
	end component;
	
	component mir is
	PORT(
		adr_in : in std_logic_vector (31 downto 0); --kompletna instrukcija
		amux : out std_logic;
		cond : out std_logic_vector (1 downto 0);
		alu : out std_logic_vector (1 downto 0);
		sh : out std_logic_vector (1 downto 0);
		mbr, mar, rd, wr, enc : out std_logic;
		c : out std_logic_vector (3 downto 0);
		b : out std_logic_vector (3 downto 0);
		a : out std_logic_vector (3 downto 0);
		adresa : out std_logic_vector (7 downto 0);
		clock : in std_logic --s_t1
	);
	end component;
	
	component rom_upravljacka is
	PORT (
		ADRESA : in std_logic_vector (7 downto 0);
		PODACI : out std_logic_vector (31 downto 0)
	);
	end component;
	
	component latch is
	port(
		d : in std_logic_vector(15 downto 0); --bus
		clk : in std_logic; --s_t2
		q : out std_logic_vector(15 downto 0) -- latch_out
		);
	end component;

	component mmux is
	port(
	kontrola : in std_logic; --izlaz iz logike
	inkrementer, adresa : inout std_logic_vector(7 downto 0); --naredna i skok adresa
	izlaz : out std_logic_vector(7 downto 0); --ulaz u rom
	clock : in std_logic --odgovarajuci ciklus
	);
	end component;

	component logika is
	port(
	cond : in std_logic_vector(1 downto 0); --iz mikroinstrukcije
	n, z : in std_logic; --flegovi iz alu
	izlaz : out std_logic --ulazi u mmux
	);
	end component;
	
	component mar is
	port(
		latch_out : in std_logic_vector(15 downto 0);
		clock, kontrola : std_logic; --mar iz instrukcije i s_t3
		mar_adresa : out  std_logic_vector (15 downto 0)
	);
	end component;
	
	component mbr is
	port(
		rd, wr, kontrola, clock : in std_logic; --rd, wr, mbr, s_t4
		c_bus : in std_logic_vector(15 downto 0);
		podaci, mbr_out : inout std_logic_vector(15 downto 0)
	);
	end component;

	signal s_mpc_reg : std_logic_vector (7 downto 0) := x"00";
	signal s_mir_reg : std_logic_vector (31 downto 0);
	
	signal c_amux : std_logic;
	signal c_cond : std_logic_vector (1 downto 0);
	signal c_alu : std_logic_vector (1 downto 0);
	signal c_sh : std_logic_vector (1 downto 0);
	signal c_mbr, c_mar, c_rd, c_wr : std_logic;
	signal c_enc : std_logic := '0';
	signal c_c : std_logic_vector (3 downto 0);
	signal c_b : std_logic_vector (3 downto 0);
	signal c_a : std_logic_vector (3 downto 0);
	signal c_adresa : std_logic_vector (7 downto 0);
	signal c_negative_flag, c_zero_flag : std_logic;
	--izlazi iz distributera
	signal s_t4, s_t3, s_t2, s_t1 : std_logic;
	--registri
	signal a_bus, b_bus : std_logic_vector (15 downto 0);
	signal c_bus: std_logic_vector (15 downto 0) := x"0000";
	signal a_latch_out, b_latch_out : std_logic_vector (15 downto 0);
	signal mbr_out : std_logic_vector (15 downto 0):= x"0000";
	
	signal amux_out, alu_out : std_logic_vector (15 downto 0);
	signal logika_out : std_logic;
	
	
begin
	-- distributer impulsa
	p_fazni_sat : distributer port map (clk, reset, s_t1, s_t2, s_t3, s_t4);
	p_upravljacka : rom_upravljacka port map (s_mpc_reg, s_mir_reg);
	p_mir : mir port map (s_mir_reg, c_amux, c_cond, c_alu, c_sh, c_mbr, c_mar, c_rd, c_wr, c_enc, c_c, c_a, c_b, c_adresa, s_t1);
	
	p_registri : registri port map (c_a, c_b, c_c, a_bus, b_bus, c_bus, c_enc, s_t4);
	a_latch : latch port map (a_bus, s_t2, a_latch_out);
	b_latch : latch port map (b_bus, s_t2, b_latch_out);
	p_amux : amux port map (c_amux, a_latch_out, mbr_out, amux_out);
	--amux_out
	p_alu : alu port map (c_alu, c_negative_flag, c_zero_flag, a_latch_out, b_latch_out, alu_out);
	p_shifter : shifter port map (alu_out, c_bus, c_sh);
	p_logika : logika port map (c_cond, c_negative_flag, c_zero_flag, logika_out);
	p_mmux : mmux port map (logika_out, s_mpc_reg, c_adresa, s_mpc_reg, s_t4);
	p_mar : mar port map (b_latch_out, s_t3, c_mar, adresa);
	p_mbr : mbr port map (c_rd, c_wr, c_mbr, s_t4, c_bus, podaci, mbr_out);
	rd <= c_rd;
	wr <= c_wr;

	
end Behavioral;

