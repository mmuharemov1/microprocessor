--Clanovi: Haris Osmanbegovic, Irhad Halilovic, Mirza Muharemovic i Rasim Sabanovic

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
--use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_SIGNED.ALL;
use IEEE.numeric_std.all;

entity alu is
port(
	ulaz : in std_logic_vector (1 downto 0);
	negative_flag, zero_flag : out std_logic;
	alatch : in std_logic_vector (15 downto 0);
	blatch : in std_logic_vector (15 downto 0);
	izlaz : out std_logic_vector (15 downto 0)
	);
end alu;

architecture Behavioral of alu is
	signal temp : std_logic_vector(15 downto 0);

begin
	process(ulaz, alatch, blatch)
	begin

			negative_flag <= '0';
			zero_flag <= '0';
			if(ulaz = "01") then
				izlaz <= alatch and blatch;
			elsif(ulaz = "10") then
				izlaz <= alatch;
			elsif(ulaz = "11") then
				izlaz <= not alatch;
			else
				izlaz <= std_logic_vector(signed(alatch) + signed(blatch));
				temp <= std_logic_vector(signed(alatch) + signed(blatch));
				negative_flag <= temp(15);
				if(temp = x"0000") then
					zero_flag <= '1';
				end if;
			end if;

	end process;

end Behavioral;

