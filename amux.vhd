--Clanovi: Haris Osmanbegovic, Irhad Halilovic, Mirza Muharemovic i Rasim Sabanovic

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity amux is
port(
	kontrola : in std_logic;
	alatch, mbr : in std_logic_vector(15 downto 0);
	izlaz : out std_logic_vector(15 downto 0)
	);
end amux;

architecture Behavioral of amux is

begin
	process(kontrola)
	begin
		if(kontrola = '0') then
			izlaz <= alatch;
		else
			izlaz <= mbr;
		end if;
	end process;
end Behavioral;

