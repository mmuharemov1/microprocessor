--Clanovi: Haris Osmanbegovic, Irhad Halilovic, Mirza Muharemovic i Rasim Sabanovic

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity latch is
port(
	d : in std_logic_vector(15 downto 0);
	clk : in std_logic;
	q : out std_logic_vector(15 downto 0)
	);
end latch;

architecture Behavioral of latch is

begin
	process(clk)
	begin
		if(clk = '1') then
			q <= d;
		end if;
	end process;

end Behavioral;

