--Clanovi: Haris Osmanbegovic, Irhad Halilovic, Mirza Muharemovic i Rasim Sabanovic

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity logika is
port(
	cond : in std_logic_vector(1 downto 0);
	n, z : in std_logic;
	izlaz : out std_logic
	);
end logika;

architecture Behavioral of logika is

begin
	process(cond, n, z)
	begin
		if(cond = "00") then
			izlaz <= '0';
		elsif (cond = "10") then
			izlaz <= z;
		elsif(cond = "01") then
			izlaz <= n;
		else
			izlaz <= '1';
		end if;
	end process;

end Behavioral;

