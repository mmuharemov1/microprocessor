--Clanovi: Haris Osmanbegovic, Irhad Halilovic, Mirza Muharemovic i Rasim Sabanovic

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity mar is
	port(
		b_latch_out : in std_logic_vector(15 downto 0);
		clock, kontrola : std_logic;
		adresa : out  std_logic_vector (15 downto 0)
	);
end mar;

architecture Behavioral of mar is
	


begin
	process(clock, kontrola)
	begin
		if(clock = '1') and (kontrola = '1') then
			adresa <= b_latch_out;
		end if;
	end process;

end Behavioral;

