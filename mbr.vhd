--Clanovi: Haris Osmanbegovic, Irhad Halilovic, Mirza Muharemovic i Rasim Sabanovic

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity mbr is
	port(
		rd, wr, kontrola, clock : in std_logic;
		c_bus : in std_logic_vector(15 downto 0);
		podaci, mbr_out : inout std_logic_vector(15 downto 0)
	);
end mbr;

architecture Behavioral of mbr is

begin
	process(rd, wr, kontrola, clock)
	begin
		if(kontrola = '1') and (clock = '1') and (rd = '1') then
			mbr_out <= podaci;
		elsif(kontrola = '1') and (clock = '1') and (wr = '1') then
			podaci <= c_bus;
		end if;
	end process;

end Behavioral;

