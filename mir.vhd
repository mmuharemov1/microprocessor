--Clanovi: Haris Osmanbegovic, Irhad Halilovic, Mirza Muharemovic i Rasim Sabanovic

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity mir is
PORT(
	adr_in : in std_logic_vector (31 downto 0);
	amux : out std_logic;
	cond : out std_logic_vector (1 downto 0);
	alu : out std_logic_vector (1 downto 0);
	sh : out std_logic_vector (1 downto 0);
	mbr, mar, rd, wr, enc : out std_logic;
	c : out std_logic_vector (3 downto 0);
	b : out std_logic_vector (3 downto 0);
	a : out std_logic_vector (3 downto 0);
	adresa : out std_logic_vector (7 downto 0);
	clock : in std_logic
);
end mir;

architecture Behavioral of mir is

begin
	process(clock)
	begin
		if(clock = '1') then
			amux <= adr_in(31);
			cond <= adr_in(30 downto 29);
			alu <= adr_in(28 downto 27);
			sh <= adr_in(26 downto 25);
			mbr <= adr_in(24);
			mar <= adr_in(23);
			rd <= adr_in(22);
			wr <= adr_in(21);
			enc <= adr_in(20);
			c <= adr_in(19 downto 16);
			b <= adr_in(15 downto 12);
			a <= adr_in(11 downto 8);
			adresa <= adr_in(7 downto 0);
		end if;
	end process;
end Behavioral;

