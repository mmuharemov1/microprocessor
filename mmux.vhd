--Clanovi: Haris Osmanbegovic, Irhad Halilovic, Mirza Muharemovic i Rasim Sabanovic

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity mmux is
port(
	kontrola : in std_logic;
	inkrementer, adresa : inout std_logic_vector(7 downto 0);
	izlaz : out std_logic_vector(7 downto 0);
	clock : in std_logic
	);
end mmux;

architecture Behavioral of mmux is

begin
	process(kontrola, clock)
	begin
		if(clock = '1') then
			if(kontrola = '0') then
				inkrementer <= inkrementer + "00000001";
				izlaz <= inkrementer;
			else
				izlaz <= adresa;
			end if;
		end if;
	end process;

end Behavioral;

