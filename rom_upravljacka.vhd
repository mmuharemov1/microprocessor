--Clanovi: Haris Osmanbegovic, Irhad Halilovic, Mirza Muharemovic i Rasim Sabanovic

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity rom_upravljacka is
PORT (
	ADRESA : in std_logic_vector (7 downto 0);
	PODACI : out std_logic_vector (31 downto 0)
);
end rom_upravljacka;

architecture Behavioral of rom_upravljacka is
TYPE romtabela IS ARRAY (0 to 1) of std_logic_vector (31 downto 0);
CONSTANT rompodaci : romtabela := (
x"61327200",
x"61327200"
);
begin
PROCESS (ADRESA)
BEGIN
	PODACI <= rompodaci (conv_integer(adresa));
END PROCESS;

end Behavioral;

