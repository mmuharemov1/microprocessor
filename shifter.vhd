--Clanovi: Haris Osmanbegovic, Irhad Halilovic, Mirza Muharemovic i Rasim Sabanovic

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity shifter is
port(
		ulaz : in std_logic_vector (15 downto 0);
		izlaz : out std_logic_vector (15 downto 0);
		s : in std_logic_vector(1 downto 0)
	);
end shifter;

architecture Behavioral of shifter is
begin

	process (ulaz, s)
	begin
		case s is
			when "00" => izlaz <= ulaz;
			when "01" => izlaz <= '0' & ulaz(15 downto 1) ; --right
			when "10" => izlaz <= ulaz(14 downto 0) & '0'; --left
			when others => null;
		end case;
	end process;

end Behavioral;

